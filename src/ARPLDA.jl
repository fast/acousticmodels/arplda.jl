module ARPLDA

using FFTW
using LinearAlgebra
using AbstractAcousticModels

export AutoRegressive,
    VectorSuffStat,
    SuffStats, 
    MatrixAutocorrelation,
    autocorrelation,
    get_matalignments

include("sstats.jl")

end
