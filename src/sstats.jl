##################################################################
# Sufficient statistics structures

struct MatrixAutocorrelation{T} <: AbstractMatrix{T}
	r::AbstractVector{T}
end

Base.size(m::MatrixAutocorrelation) = (length(m.r)-1, length(m.r)-1)
Base.getindex(m::MatrixAutocorrelation{T}, i, j) where T = m.r[abs(i-j) + 1]
Base.:+(x::MatrixAutocorrelation, y::MatrixAutocorrelation) = MatrixAutocorrelation(x.r + y.r)
Base.:*(x::MatrixAutocorrelation{T}, a::Number) where T  = MatrixAutocorrelation{T}(x.r * a)
Base.:*(a::Number, x::MatrixAutocorrelation{T}) where T  = MatrixAutocorrelation{T}(a * x.r)

struct SuffStats{T} 
	N::Int64
	r₀::T
	r::Vector{T}
	R⁻::MatrixAutocorrelation{T}
end
SuffStats(r::AbstractVector) = SuffStats(1, r[1], r[2:end], MatrixAutocorrelation(r))
Base.length(s::SuffStats) = 1

Base.:*(a::Number, x::SuffStats{T}) where T = SuffStats{T}(a * x.N, a * x.r₀, a * x.r, a * x.R⁻)
Base.:*(x::SuffStats{T}, a::Number) where T  = SuffStats{T}(x.N * a, x.r₀ * a, x.r * a, x.R⁻ * a)  
Base.:+(x::SuffStats, y::SuffStats) = SuffStats(x.N + y.N, x.r₀ + y.r₀, x.r + y.r, x.R⁻ + y.R⁻)

##################################################################
# ARPLDA acoustic model functions


struct AutoRegressive <: AbstractAcousticModels.AbstractAcousticModel
	features
	filters
	G::Real
end


function AbstractAcousticModels.numpdfs(m::AutoRegressive)
    return size(m.filters, 2)
end


function AbstractAcousticModels.params(m::AutoRegressive)
	return Dict(
		"gain" => m.G,
		"nfilters" => numpdfs(m),
		"k" => size(m.filters, 1)
	)
end


function autocorrelation(sig, m, fs)
	"""
	Compute autocorrelation (using the inverse fourier transform of the signal energy)
	"""
	k = params(m)["k"]
    framesize = Int(m.features.config["frameduration"] * fs)
	next_pow = Int(2^ceil(log2(framesize)))
	r = irfft(abs.(m.features(sig, fs)).^2, next_pow, 1)[1:k+1, :]
    return r
end


function AbstractAcousticModels.loglikelihood(m::AutoRegressive, v::SuffStats, fs, filters=(1, size(m.filters, 2)))  
	""""
	Compute likelihood for a given frame and given filters
		- filters : list of filters index	
	"""
	
	κ = 1
	T = m.features.config["frameduration"] * fs

	size(v.r, 1) == params(m)["k"] || throw(ArgumentError("Filters and sufficient statistics should share the same dimension K"))
	length(filters) <= size(m.filters, 2) || throw(ArgumentError("Filters index arguments should be included in the model filters"))
	res = zeros(length(filters))

	# For each filter given by the model
	for (i, idx) in enumerate(filters)
		aᵥ = m.filters[:, idx]
		q = v.r₀ - 2 * dot(aᵥ, v.r) + dot(aᵥ, v.R⁻, aᵥ)
		res[i] = -v.N*T/2 * log(2*π) + v.N*T/2 * log(κ) - κ/2 * q
	end

	res
end


function get_matalignments(m, sig, alignments, phn2idx, fs=16000)
	"""
	Compute boolean alignments matrix (timeframes by labels)
	Input :
		- m : AutoRegressivePLDA model
		- sig : signal samples
		- alignments : Alignments loaded in corpus Annotation Data 
	"""
	
	# Compute autocorrelation
	r = autocorrelation(sig, m, fs)

	stride = Int(m.features.config["framestep"] * fs)
	_, N = size(r)
	matalign = zeros(N, length(phn2idx))
	
	# Fill matrix
	for (start, stop, phoneme) in alignments
		fstart = start ÷ stride + 1
		fstop = min(stop ÷ stride, N)
		matalign[fstart:fstop,phn2idx[phoneme]] .= 1
	end
	matalign
end


# function AbstractAcousticModels.update()
# 	return
# end