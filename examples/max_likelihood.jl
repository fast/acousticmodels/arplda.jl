### A Pluto.jl notebook ###
# v0.19.40

using Markdown
using InteractiveUtils

# ╔═╡ 19e2a20c-e831-11ee-25e2-210e268523bd
begin
	using Pkg
	Pkg.activate("..")
	using Revise

	using ARPLDA
	using SpeechDatasets
	using SpeechFeatures
	using AbstractAcousticModels
	using LinearAlgebra

	using Statistics
	using Plots
end

# ╔═╡ 9ec093dc-12a2-40fd-8ffa-91fc05438deb
md"""
## Get 5 audio samples from one speaker
"""

# ╔═╡ da44cebd-43ed-4b4f-a5d7-83b388525afd
dev = TIMIT("/vol/corpora1/data/ldc/ldc1993s01/", "datasets/timit", :dev)

# ╔═╡ 29e46708-7cdf-4a9b-b19e-a3a73e01a6be
begin
	spkid = "mlsh0"
	annot_ids = [key for key in keys(dev.annotations) if startswith(key, "timit_$spkid")][1:5]
	spk_annot = Dict(key => dev.annotations[key] for key in annot_ids)
end

# ╔═╡ 62ee0af6-2073-4105-97b4-dbd52b03beac
annot_ids

# ╔═╡ d3e1c76b-52ca-46a5-9648-20a16fe13bf4
md"""
### Get phone labels
"""

# ╔═╡ d50552c4-273c-474c-b3bb-d5dfdd046465
begin 
	phones = []
	for (_, v) in spk_annot
		push!(phones, [alignment[3] for alignment in v.data["alignment"]])
	end
	labels = collect(Set(vcat(phones...)))
end

# ╔═╡ 1e9d552c-e5e5-4d0f-a7e5-8d5135ba2a60
md"""
39 phone labels (without [SIL] and [""]):
"""

# ╔═╡ 971bcf13-166a-4e79-94cc-7ae9df2bf74c
flabels = filter(x -> x != "sil" && x != "", labels)

# ╔═╡ 07367c0f-1699-4395-a0ad-5d1a6f6797ea
function fill_idx(lst::Vector{T}) where T
    dict = Dict{T, Int}()
    for (index, value) in enumerate(lst)
        dict[value] = index
    end
    return dict
end

# ╔═╡ 9acffbef-c695-4b89-bb0c-841173c7b9f3
lidx = fill_idx(labels)

# ╔═╡ d6a39f05-6599-4c43-a8af-ef9e2cb9e6e6
md"""
### Load the AutoRegressive Model
"""

# ╔═╡ 2419f455-cd22-414a-945f-5ef1de223fc3
begin
	fs = 16000
	fea = STFT(fs)
	m = AutoRegressive(fea, rand(12, length(labels)), 1)
end

# ╔═╡ e370c6cb-ca74-4205-b1b0-8747b7df4085
m.filters

# ╔═╡ d80fce39-5573-4c17-b032-80c77648119e
first(spk_annot).second.data["text"]

# ╔═╡ 66acd42c-c0cd-421f-814c-858af6f13a4e
md"""
#### Sparse alignment matrices
"""

# ╔═╡ 32146aae-62e8-470a-8e1a-af9e292b374f
begin 
	idx = first(annot_ids)
	(channel, _), _ = dev[idx]
	align = dev.annotations[idx].data["alignment"]
end

# ╔═╡ 28a4cad2-c364-49a9-acf2-9a50e952f5d8
matalign = get_matalignments(m, channel[:, 1], align, lidx)

# ╔═╡ 175e7f69-60e4-4ffb-a3fd-d779e5cf4b15
size(matalign)

# ╔═╡ 3f756c7a-1671-4b2f-acae-c9f6fab01dbd
heatmap(matalign', yticks=(1:length(Set(labels)), labels), size=(1200, 600),  titlefont = font(12,"Computer Modern"), title=("Alignment for "*first(annot_ids)))

# ╔═╡ 0904ec28-7f4f-41b6-93c9-cf604660e09c
md"""
### Compute likelihood according to alignments
"""

# ╔═╡ dbd1ddb1-2736-416e-8651-4942c4569420
begin
	dsuffstats = Dict()
	r = autocorrelation(channel[:, 1], m, fs)
	_, N = size(r)
	S = SuffStats.(eachcol(r))
	dsuffstats = [sum(S .* matalign[:,lidx[phone]]) for phone in labels]	
	dsuffstats
end

# ╔═╡ 9fb46af7-1e02-4461-adde-05aed02b3ad2
begin 
	filters = 1:10
	llhframes = reduce(hcat, loglikelihood.(Ref(m), S, Ref(fs), Ref(filters)))
end

# ╔═╡ ea61aa53-4ea8-4ff7-9dc3-166bdc432dcd
heatmap(llhframes, c=:YlOrBr, xlabel="Frames", label="Filters",title=first(annot_ids), yticks=(filters, labels[filters]),  titlefont = font(12,"Computer Modern")) #aspect_ratio=:equal)

# ╔═╡ 93f1bed4-3684-42ea-a7d9-3e08802b08c6
loglikelihood.(Ref(m), dsuffstats, Ref(fs))

# ╔═╡ 00009039-6f68-41dd-b543-e26d015245e0
begin
	x = 5
	llhtot = zeros(length(dsuffstats), x)
	for (i, v) in enumerate(dsuffstats)
		llhtot[i, :] .= loglikelihood(m, v, fs, [i])[1]
	end
	heatmap(llhtot, yticks=(1:length(Set(labels)), labels), aspect_ratio=:equal, size=(400, 500), xlim=(0,x), xtickfontcolor=:white, title="Likelihood for every class with alignments", titlefont = font(12,"Computer Modern"))
end

# ╔═╡ 935f1f95-7a21-4a18-8ab2-9c5c1b4bfff5


# ╔═╡ 6aa65710-e718-4bc5-8133-88d9319266a6


# ╔═╡ 8359ed59-d229-41d0-bb00-25ba55a6b27b
md"""
#### Maximum likelihood
```math
\begin{align}
\hat{\mathbf{a}_v}
&= \arg \max_{\mathbf{a}_v} p(\mathbf{x}_{v}|\mathbf{a}_{v}) \\
&= \arg \max_{\mathbf{a}_v} \prod_{i=1}^{N} p(\mathbf{x}_{v_i} | \mathbf{a}_{v}) \\
&= \arg \max_{\mathbf{a}_v} \exp \Big( -\frac{NT}{2}\ln(2\pi) + \frac{NT}{2}\ln(\kappa) - \frac{\kappa}{2} \sum_{i=1}^{N} (\underbrace{\mathbf{x}_{v_i}^\top\mathbf{x}_{v_i}}_{R_{v_i}(0)} - \underbrace{\mathbf{x}_{v_i}^\top\boldsymbol{\mu}_{v} - \boldsymbol{\mu}_{v}^\top\mathbf{x}_{v_i}}_{2 \ \mathbf{a}_{v}^\top\mathbf{r}_{v_i}} + \underbrace{\boldsymbol{\mu}_{v}^\top\boldsymbol{\mu}_v}_{\mathbf{a}_v^\top \mathbf{R}_{v_i}^{-} \mathbf{a}_{v}})\Big) \\
\end{align}
```
The log likelihood becomes :
```math
\begin{align}
L = -\frac{NT}{2}\ln(2\pi) + \frac{NT}{2} \ln(\kappa) - \frac{\kappa}{2} \sum_{i=1}^{N} (\mathbf{x}_{v_i}^\top\mathbf{x}_{v_i} - \mathbf{x}_{v_i}^\top\boldsymbol{\mu}_{v} - \boldsymbol{\mu}_{v}^\top\mathbf{x}_{v_i}+ \boldsymbol{\mu}_{v}^\top\boldsymbol{\mu}_v)
\end{align}
```
Differentiation with respect to ``μ`` :
```math
\begin{align}
\frac{\partial L}{\partial \boldsymbol{\mu}_{v}} 
= - \frac{\kappa}{2} \sum_{i=1}^{N} (-2 \mathbf{x}_{v_i} + 2 \boldsymbol{\mu}_{v})
= - \kappa \sum_{i=1}^{N} (- \mathbf{x}_{v_i} + \boldsymbol{\mu}_{v})
= 0
\end{align}
```

```math
\begin{align}
\hat{\boldsymbol{\mu}_{v}} = \frac{\sum_{i=1}^{N} \mathbf{x}_{v_i}}{N}
\end{align}
```

By a similar process, the differentiation with respect to ``κ`` :

```math
\begin{align}
\hat{κ_v}^{-1} = \hat{G_v}^2 = \frac{1}{NT} \sum_{i=1}^{N} 
\Big( 
\mathbf{x}_{v_i}^\top\mathbf{x}_{v_i} - \mathbf{x}_{v_i}^\top\boldsymbol{\mu}_{v} - \boldsymbol{\mu}_{v}^\top\mathbf{x}_{v_i}+ \boldsymbol{\mu}_{v}^\top\boldsymbol{\mu}_v
\Big)
\end{align}
```

"""

# ╔═╡ ac10f671-66c4-4b59-aa1b-179487a397c5


# ╔═╡ Cell order:
# ╠═19e2a20c-e831-11ee-25e2-210e268523bd
# ╟─9ec093dc-12a2-40fd-8ffa-91fc05438deb
# ╠═da44cebd-43ed-4b4f-a5d7-83b388525afd
# ╠═29e46708-7cdf-4a9b-b19e-a3a73e01a6be
# ╠═62ee0af6-2073-4105-97b4-dbd52b03beac
# ╟─d3e1c76b-52ca-46a5-9648-20a16fe13bf4
# ╠═d50552c4-273c-474c-b3bb-d5dfdd046465
# ╟─1e9d552c-e5e5-4d0f-a7e5-8d5135ba2a60
# ╠═971bcf13-166a-4e79-94cc-7ae9df2bf74c
# ╟─07367c0f-1699-4395-a0ad-5d1a6f6797ea
# ╠═9acffbef-c695-4b89-bb0c-841173c7b9f3
# ╟─d6a39f05-6599-4c43-a8af-ef9e2cb9e6e6
# ╠═2419f455-cd22-414a-945f-5ef1de223fc3
# ╠═e370c6cb-ca74-4205-b1b0-8747b7df4085
# ╟─d80fce39-5573-4c17-b032-80c77648119e
# ╟─66acd42c-c0cd-421f-814c-858af6f13a4e
# ╠═32146aae-62e8-470a-8e1a-af9e292b374f
# ╠═28a4cad2-c364-49a9-acf2-9a50e952f5d8
# ╠═175e7f69-60e4-4ffb-a3fd-d779e5cf4b15
# ╟─3f756c7a-1671-4b2f-acae-c9f6fab01dbd
# ╠═9fb46af7-1e02-4461-adde-05aed02b3ad2
# ╟─ea61aa53-4ea8-4ff7-9dc3-166bdc432dcd
# ╟─0904ec28-7f4f-41b6-93c9-cf604660e09c
# ╠═dbd1ddb1-2736-416e-8651-4942c4569420
# ╠═93f1bed4-3684-42ea-a7d9-3e08802b08c6
# ╟─00009039-6f68-41dd-b543-e26d015245e0
# ╠═935f1f95-7a21-4a18-8ab2-9c5c1b4bfff5
# ╠═6aa65710-e718-4bc5-8133-88d9319266a6
# ╟─8359ed59-d229-41d0-bb00-25ba55a6b27b
# ╠═ac10f671-66c4-4b59-aa1b-179487a397c5
